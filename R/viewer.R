
#' Define filenames for creating Views
#'
#' @export
setOutputImageName <- function(namePattern = "test%03d") {
  while (!is.null(dev.list()))  dev.off()
  if(suppressWarnings(require('Cairo',quietly=TRUE))) {
    CairoPNG(filename = paste0(namePattern,".png"), units='px', width=300,height=300,pointsize=12)
  } else {
    png(filename = paste0(namePattern,".png"), units='px', width=300,height=300,pointsize=12)
  }
}
